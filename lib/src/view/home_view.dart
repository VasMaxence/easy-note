import 'package:easy_note/src/view/component/auto_size_text.dart';
import 'package:easy_note/src/view_model/NotesViewModel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomeView extends StatelessWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        bottom: false,
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Container(
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            child: Icon(
                              Icons.note_add_rounded,
                              color: Colors.transparent,
                            ),
                          ),
                          Material(
                            type: MaterialType.transparency,
                            child: Hero(
                              tag: 'app_name',
                              child: AutoSizeText(
                                'Easy Note',
                                maxLines: 1,
                                minFontSize: 22,
                                style: TextStyle(
                                  decoration: TextDecoration.none,
                                  fontFamily: 'Baloo-SemiBold',
                                  color: Theme.of(context)
                                      .textTheme
                                      .headline1
                                      ?.color,
                                  fontSize: 25,
                                ),
                              ),
                            ),
                          ),
                          GestureDetector(
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: Icon(
                                Icons.note_add_rounded,
                                color: Theme.of(context)
                                    .textTheme
                                    .headline1
                                    ?.color,
                              ),
                            ),
                            onTap: () => null,
                          ),
                        ],
                      ),
                      Divider(),
                    ],
                  ),
                ),
              ),
              Builder(builder: (context) {
                return SingleChildScrollView(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Consumer<NotesViewModel>(
                      builder: (context, notes, _) {
                        return Column(
                          children: notes.notes
                              .map((e) => e.generateVisualizer(context))
                              .toList(),
                        );
                      },
                    ),
                  ),
                );
              }),
            ],
          ),
        ),
      ),
    );
  }
}