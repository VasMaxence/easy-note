import 'package:flutter/material.dart';

abstract class Note {
  String? title;
  IconData? icon;
  DateTime? finalDate;

  Note({
    this.title,
    this.icon,
    this.finalDate,
  });

  Widget generateVisualizer(BuildContext context);

  Widget generateBody(BuildContext context);

  Widget generatePage(BuildContext context);
}
