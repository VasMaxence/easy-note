import 'package:easy_note/src/view/component/note/abstract_note.dart';
import 'package:flutter/material.dart';

class BasicNote extends Note {
  @override
  Widget generateBody(BuildContext context) {
    return Container();
  }

  @override
  Widget generatePage(BuildContext context) {
    return Container();
  }

  @override
  Widget generateVisualizer(BuildContext context) {
    throw UnimplementedError();
  }

}