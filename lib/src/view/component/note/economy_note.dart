import 'package:easy_note/src/view_model/NotesViewModel.dart';
import 'package:easy_note/src/view_model/notes_view_model/economy_notes_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:easy_note/src/view/component/note/abstract_note.dart';
import 'package:easy_note/src/view/component/auto_size_text.dart';

class EconomyNote extends Note {
  List<EconomyOperation> data;

  EconomyNote({
    String? title,
    IconData? icon,
    DateTime? finalDate,
    required this.data,
  }) : super(
          title: title,
          icon: icon ?? Icons.monetization_on_outlined,
          finalDate: finalDate,
        );

  @override
  Widget generateBody(BuildContext context) {
    throw UnimplementedError();
  }

  @override
  Widget generatePage(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => EconomyNotesViewModel(data: data),
      builder: (context, _) {
        return Scaffold(
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              final t =
              Provider.of<EconomyNotesViewModel>(context, listen: false);
              t.addOperation(EconomyOperation(
                  type: EconomyType.debit, value: 69, name: 'test'));
              Provider.of<NotesViewModel>(context, listen: false).update();
            },
          ),
          body: SafeArea(
            bottom: false,
            child: Column(
              children: [
                Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              GestureDetector(
                                child: Icon(
                                  Icons.arrow_back_ios_rounded,
                                  color: Theme.of(context)
                                      .textTheme
                                      .headline1
                                      ?.color,
                                ),
                                onTap: () => Navigator.pop(context),
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 10),
                                child: Material(
                                  type: MaterialType.transparency,
                                  child: Hero(
                                    tag: 'app_name',
                                    child: AutoSizeText(
                                      'Easy Note',
                                      maxLines: 1,
                                      minFontSize: 22,
                                      style: TextStyle(
                                        decoration: TextDecoration.none,
                                        fontFamily: 'Baloo-SemiBold',
                                        color: Theme.of(context)
                                            .textTheme
                                            .headline1
                                            ?.color,
                                        fontSize: 22,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          AutoSizeText(
                            'OK',
                            maxLines: 1,
                            minFontSize: 18,
                            style: TextStyle(
                              decoration: TextDecoration.none,
                              fontFamily: 'Baloo-SemiBold',
                              color: Theme.of(context).textTheme.headline1?.color,
                              fontSize: 20,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Divider(),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                  child: Consumer<EconomyNotesViewModel>(
                    builder: (context, note, _) {
                      return SingleChildScrollView(
                        child: Column(
                          children: note.data
                              .map((e) =>
                              Text(e.name + ' : ' + e.value.toString()))
                              .toList(),
                        ),
                      );
                    },
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  Widget generateVisualizer(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        vertical: 5,
      ),
      child: GestureDetector(
        child: Container(
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Theme.of(context).backgroundColor,
            boxShadow: [
              BoxShadow(
                color: Theme.of(context).shadowColor,
                offset: Offset(0, 2),
                spreadRadius: -1,
                blurRadius: 8,
              ),
            ],
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 12, horizontal: 20),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Icon(
                      super.icon,
                      color: Theme.of(context).textTheme.headline2?.color,
                      size: 22,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width * .5,
                        child: AutoSizeText(
                          super.title ?? '',
                          maxLines: 2,
                          minFontSize: 14,
                          style: TextStyle(
                            color: Theme.of(context).textTheme.headline2?.color,
                            fontFamily: 'Baloo-Regular',
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                AutoSizeText(
                  getTotal().toString() + '€',
                  maxLines: 1,
                  minFontSize: 14,
                  style: TextStyle(
                    color:
                        getTotal() < 0 ? Color(0xffED7A6E) : Color(0xffC0DC77),
                    fontFamily: 'Baloo-SemiBold',
                    fontSize: 18,
                  ),
                ),
              ],
            ),
          ),
        ),
        onTap: () => Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => this.generatePage(context))),
      ),
    );
  }

  double getTotal() {
    double total = 0;
    data.forEach((element) {
      total = element.calculate(total, element);
    });
    return total;
  }
}

enum EconomyType {
  credit,
  debit,
}

class EconomyOperation {
  final EconomyType type;
  double value;
  double quantity;
  String name;

  EconomyOperation({
    required this.type,
    required this.value,
    required this.name,
    this.quantity = 1,
  });

  double calculate(double actual, EconomyOperation operation) {
    switch (operation.type) {
      case EconomyType.credit:
        actual += operation.value;
        break;
      case EconomyType.debit:
        actual -= operation.value;
        break;
    }
    return actual;
  }
}
