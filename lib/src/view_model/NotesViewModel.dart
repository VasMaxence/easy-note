import 'package:easy_note/src/view/component/note/abstract_note.dart';
import 'package:easy_note/src/view/component/note/economy_note.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class NotesViewModel extends ChangeNotifier {
  late List<Note> notes;

  NotesViewModel() {
    notes = [
      EconomyNote(
        title: 'Test Economy',
        finalDate: DateTime(2022, 3, 13),
        data: [],
      ),
      EconomyNote(
        title: 'Une application ça coute chers',
        finalDate: DateTime(2022, 3, 13),
        data: [],
      ),
      EconomyNote(
        title: 'Coûts Maison',
        finalDate: DateTime(2022, 3, 13),
        data: [
          EconomyOperation(
              type: EconomyType.debit, value: 1220, name: 'Electricity'),
          EconomyOperation(
              type: EconomyType.debit, value: 869, name: 'Plomberie'),
        ],
      ),
    ];
  }

  void update() => notifyListeners();
}
