import 'package:easy_note/src/view/component/note/economy_note.dart';
import 'package:flutter/cupertino.dart';

class EconomyNotesViewModel extends ChangeNotifier {
  List<EconomyOperation> data;

  EconomyNotesViewModel({required this.data});

  void addOperation(EconomyOperation operation) {
    data.add(operation);
    notifyListeners();
  }
}