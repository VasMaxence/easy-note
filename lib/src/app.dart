import 'package:easy_note/src/view/home_view.dart';
import 'package:easy_note/src/view_model/NotesViewModel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => NotesViewModel()),
      ],
      child: MaterialApp(
        themeMode: ThemeMode.system,
        theme: ThemeData(
          brightness: Brightness.light,
          scaffoldBackgroundColor: Color(0xffFFFFFF),
          backgroundColor: Color(0xffF0F4F8),
          shadowColor: Colors.grey,
          textTheme: TextTheme(
            headline1: TextStyle(
              color: Colors.black,
              fontFamily: 'Baloo-SemiBold',
              fontSize: 28,
            ),
          ),
        ),
        darkTheme: ThemeData(
          brightness: Brightness.dark,
          scaffoldBackgroundColor: Color(0xff404e5a),
          backgroundColor: Color(0xff4E5D6A),
          shadowColor: Color(0xff404e5a),
          textTheme: TextTheme(
            headline1: TextStyle(
              color: Color(0xffffbe0b),
              fontFamily: 'Baloo-SemiBold',
              fontSize: 28,
            ),
          ),
        ),
        debugShowCheckedModeBanner: false,
        home: HomeView(),
      ),
    );
  }
}
